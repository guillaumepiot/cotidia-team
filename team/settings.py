from django.conf import settings

# The default page templates
TEAM_TEMPLATES = getattr(settings, 'TEAM_TEMPLATES', (('team/member.html', 'Default member\'s page'),))

# If set to True, you can relate team members to pages, and use the members_for_page templatetags to get members inside a page
TEAM_RELATE_MEMBER_TO_PAGES = getattr(settings, 'TEAM_RELATE_MEMBER_TO_PAGES', False)