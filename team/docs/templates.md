Templates
=========

Template tags
-------------



`get_all_members` populate the template context with all the team members.

	{% get_all_members page as members %}
	
Eg:
	
	{% get_all_members as members %}
	{% for member in members %}
		{{member}}
	{% endfor %}
	
	
	
`members_for_page` populate the template context with a list of members related to a specific page.

	{% members_for_page page as members %}
	
Eg:
	
	{% members_for_page page as members %}
	{% for member in members %}
		{{member}}
	{% endfor %}
	
	
