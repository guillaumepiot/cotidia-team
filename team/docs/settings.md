Settings
========



`TEAM_TEMPLATES` Default = (('team/member.html', 'Default member\'s page'),)

The default member page templates



`TEAM_RELATE_MEMBER_TO_PAGES` Default = False

If set to True, you can relate team members to pages, and use the members_for_page templatetags to get members inside a page