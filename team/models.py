from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from localeurl.models import reverse

from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from multilingual_model.models import MultilingualModel, MultilingualTranslation
from cmsbase.models import BasePage, PublishTranslation, BasePageManager, Page


from team import settings as team_settings


# Subclass the PageTranslation model to create the member translation

class MemberTranslation(MultilingualTranslation, PublishTranslation):
	parent = models.ForeignKey('Member', related_name='translations')
	title = models.CharField(_('Full name'), max_length=100)
	slug = models.SlugField(max_length=100)
	role = models.CharField(_('Role'), max_length=100, blank=True)
	role_description = models.TextField(_('Role description'), max_length=100, blank=True)
	dept = models.CharField(_('Department'), max_length=100, blank=True)
	content = models.TextField(blank=True)

	#Meta data
	meta_title = models.CharField(max_length=100, blank=True)
	meta_description = models.TextField(blank=True)

	class Meta:
		unique_together = ('parent', 'language_code')

		if len(settings.LANGUAGES) > 1:
			verbose_name=_('Translation')
			verbose_name_plural=_('Translations')
		else:
			verbose_name=_('Content')
			verbose_name_plural=_('Content')

	def __unicode__(self):
		return dict(settings.LANGUAGES).get(self.language_code)


class MemberManager(BasePageManager):
	pass

# Subclass the Page model to create the member model

class Member(BasePage):
	# Optional link to user in CMS
	user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
	# Extra fields
	categories = models.ManyToManyField('Category', blank=True)

	email = models.EmailField(_('Email'), max_length=100, blank=True)
	phone = models.CharField(_('Phone number'), max_length=100, blank=True)

	# Social networks
	twitter = models.CharField(_('Twitter'), max_length=100, blank=True, help_text=_('Enter the Twitter username'))
	linkedin = models.CharField(_('LinkedIn'), max_length=100, blank=True, help_text=_('Enter the LinkedIn profile url'))
	behance = models.CharField(_('Behance'), max_length=100, blank=True, help_text=_('Enter the Behance profile url'))

	# Image
	image = models.ImageField(upload_to='team', max_length=100, blank=True)

	related_to_pages = models.ManyToManyField(Page, blank=True)
	
	# Manager
	objects = MemberManager()

	class Meta:
		verbose_name=_('Member')
		verbose_name_plural=_('Members')
		ordering = ['order_id']

	class CMSMeta:
	
		# A tuple of templates paths and names
		templates = team_settings.TEAM_TEMPLATES
		
		# Indicate which Translation class to use for content
		translation_class = MemberTranslation

		# Provide the url name to create a url for that model
		model_url_name = 'team:member'


	def delete(self, *args, **kwargs):
		if self.image:
			storage, path = self.image.storage, self.image.path
		super(Member, self).delete(*args, **kwargs)
		# Physically delete the file
		if self.image:
			storage.delete(path)
		
# Team categories

class CategoryTranslation(MultilingualTranslation):
	parent = models.ForeignKey('Category', related_name='translations')
	title = models.CharField(_('Category title'), max_length=100)
	slug = models.SlugField(max_length=100)

	class Meta:
		unique_together = ('parent', 'language_code')

		if len(settings.LANGUAGES) > 1:
			verbose_name=_('Translation')
			verbose_name_plural=_('Translations')
		else:
			verbose_name=_('Content')
			verbose_name_plural=_('Content')

	def __unicode__(self):
		return dict(settings.LANGUAGES).get(self.language_code)

	

class Category(MultilingualModel):
	identifier = models.SlugField(max_length=100)
	published = models.BooleanField(_('Active'))
	order_id = models.IntegerField()

	class Meta:
		verbose_name=_('Category')
		verbose_name_plural=_('Categories')

	class CMSMeta:
		translation_class = CategoryTranslation

	def __unicode__(self):
		return self.unicode_wrapper('title', default='Unnamed')

	def get_translations(self):
		return self.CMSMeta.translation_class.objects.filter(parent=self)

	def translated(self):
		from django.utils.translation import get_language

		try:
			translation = self.CMSMeta.translation_class.objects.get(language_code=get_language(), parent=self)
			return translation
		except:
			return self.CMSMeta.translation_class.objects.get(language_code=settings.LANGUAGE_CODE, parent=self)

	def get_absolute_url(self):
		return reverse('blog:category', kwargs={'slug':self.translated().slug})