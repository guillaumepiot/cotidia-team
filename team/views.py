from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from localeurl.models import reverse
from django.conf import settings

from cmsbase.views import page_processor

from team.models import Member, MemberTranslation, Category, CategoryTranslation
from team import settings as team_settings


@page_processor(model_class=Member, translation_class=MemberTranslation)
def member(request, member, slug):

	if not member:
		template = team_settings.team_TEMPLATES[0][0]
	else:
		template = member.template

	return render_to_response(template, {'member':member,}, context_instance=RequestContext(request))

def team(request):
	members = Member.objects.get_published_live()
	return render_to_response('team/team.html', {'members':members}, context_instance=RequestContext(request))

def categories(request):
	return render_to_response('team/categories.html', {}, context_instance=RequestContext(request))

def category(request, slug):
	_category_translation = get_object_or_404(CategoryTranslation, slug=slug, parent__published=True)
	_category = _category_translation.parent
	members = Member.objects.get_published_live().filter(published_from__categories=_category)
	return render_to_response('team/category.html', {'category':_category, 'members':members}, context_instance=RequestContext(request))
