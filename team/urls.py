from django.conf.urls import patterns, include, url

urlpatterns = patterns('team',

	# Default
	url(r'^$', 'views.team', name="team"),

	# Categories
	url(r'^categories/$', 'views.categories', name="categories"),
	url(r'^category/(?P<slug>[-\w]+)/$', 'views.category', name="category"),

	# Article view
	url(r'^(?P<slug>[-\w\/]+)/$', 'views.member', name="member"),

)