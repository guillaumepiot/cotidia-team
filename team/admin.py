import reversion

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _
from django.conf import settings
from django.contrib.admin.views.main import ChangeList

from mptt.admin import MPTTModelAdmin
from multilingual_model.admin import TranslationInline

from redactor.widgets import RedactorEditor

from cmsbase.admin import PageAdmin, PageFormAdmin, PublishingWorkflowAdmin
from cmsbase.widgets import AdminImageWidget, AdminCustomFileWidget
from cmsbase.admin_forms import TranslationForm

from team.models import *
from team import settings as team_settings


# Member translation

class MemberTranslationInlineFormAdmin(forms.ModelForm):
	slug = forms.SlugField(label=_('member URL'))
	content = forms.CharField(widget=RedactorEditor(redactor_css="/static/css/redactor-editor.css"), required=False)
	role_description = forms.CharField(widget=RedactorEditor(redactor_css="/static/css/redactor-editor.css"), required=False)

	class Meta:
		model = MemberTranslation

	def has_changed(self):
		""" Should returns True if data differs from initial.
		By always returning true even unchanged inlines will get validated and saved."""
		return True

class MemberTranslationInline(TranslationInline):
	model = MemberTranslation
	form = MemberTranslationInlineFormAdmin
	extra = 0 if settings.PREFIX_DEFAULT_LOCALE else 1
	prepopulated_fields = {'slug': ('title',)}
	# template = 'admin/cmsbase/cms_translation_inline.html'


# Member

class MemberAdminForm(PageFormAdmin):
	image = forms.ImageField(label=_('Image'), widget=AdminImageWidget, required=False)
	categories = forms.ModelMultipleChoiceField(queryset=Category.objects.filter(), widget=forms.CheckboxSelectMultiple, required=False)
	class Meta:
		model = Member

	def __init__(self, *args, **kwargs):
		super(MemberAdminForm, self).__init__(*args, **kwargs)
		
		if team_settings.TEAM_RELATE_MEMBER_TO_PAGES:
			self.fields['related_to_pages'] = forms.ModelMultipleChoiceField(queryset=Page.objects.get_published_original(), widget=forms.CheckboxSelectMultiple, required=False)
		

class MemberAdmin(reversion.VersionAdmin, PublishingWorkflowAdmin):
	# form = MemberAdminForm
	translation_form_class = TranslationForm

	inlines = (MemberTranslationInline, )

	# Override the list display from PublishingWorkflowAdmin
	def get_list_display(self, request, obj=None):
		if not settings.PREFIX_DEFAULT_LOCALE:
			return ['display_title', 'is_published', 'approval', '_categories', 'order_id', 'template']
		else:
			return ['display_title', 'is_published', 'approval', '_categories', 'order_id', 'template', 'languages']

	def _categories(self, obj):
		cats = [c.title for c in obj.categories.all()]
		return ', '.join(cats)

	_categories.allow_tags = True
	_categories.short_description = 'Categories'

	fieldsets = (
		('Settings', {
			#'description':_('The page template'),
			'classes': ('default',),
			'fields': ('template', 'user', 'order_id', 'display_title', 'slug', 'email', 'phone', 'twitter', 'linkedin', 'behance', 'image', 'categories')
		}),
	)

	if team_settings.TEAM_RELATE_MEMBER_TO_PAGES:
		fieldsets = fieldsets + (('Relate to pages', {
			'classes': ('default hide-labels',),
			'fields': ( 'related_to_pages',)
		}),)


	class Media:
		css = {
			"all": ("admin/css/page.css",)
		}
		js = ("admin/js/page.js",)

admin.site.register(Member, MemberAdmin)





# Category translation

class CategoryTranslationInline(TranslationInline):
	model = CategoryTranslation
	extra = 0 if settings.PREFIX_DEFAULT_LOCALE else 1
	prepopulated_fields = {'slug': ('title',)}
	template = 'admin/cmsbase/cms_translation_inline.html'



# Category

class CategoryAdmin(admin.ModelAdmin):

	list_display = ["title", "identifier", "published", 'order_id', 'languages']
	inlines = (CategoryTranslationInline, )

	def title(self, obj):
		translation = obj.translated() #PageTranslation.objects.filter(parent=obj, language_code=settings.DEFAULT_LANGUAGE)
		if translation:
			return translation.title
		else:
			return _('No translation available for default language')

	def languages(self, obj):
		ts=[]
		for t in obj.get_translations():
			ts.append(u'<img src="/static/admin/img/flags/%s.png" alt="" rel="tooltip" data-title="%s">' % (t.language_code, t.__unicode__()))
		return ' '.join(ts)

	languages.allow_tags = True
	languages.short_description = 'Translations'

	# Override the list display from PublishingWorkflowAdmin
	def get_list_display(self, request, obj=None):
		if not settings.PREFIX_DEFAULT_LOCALE:
			return ["title", "identifier", "published", 'order_id']
		else:
			return ["title", "identifier", "published", 'order_id', 'languages']

	fieldsets = (

		
		('Settings', {
			#'description':_('The page template'),
			'classes': ('default',),
			'fields': ('published', 'order_id', 'identifier', )
		}),

	)

	class Media:
		css = {
			"all": ("admin/css/page.css",)
		}
		js = ("admin/js/page.js",)

admin.site.register(Category, CategoryAdmin)