from django import template
register = template.Library()

from team.models import Member, Category
from team import settings as team_settings

@register.inclusion_tag('team/includes/_nav.html')
def team_nav():
    members = Member.objects.get_published_live()
    return {'members': members}

@register.inclusion_tag('team/includes/_categories.html')
def team_categories():
    categories = Category.objects.filter(published=True)
    return {'categories': categories}


@register.assignment_tag
def members_for_page(page=False):
    if page:
        if page.published_from:
            members = Member.objects.get_published_live().filter(published_from__related_to_pages=page.published_from).order_by('published_from__order_id')
        else:
            members = Member.objects.get_published_live().filter(published_from__related_to_pages=page).order_by('published_from__order_id')
    else:
        members = Member.objects.get_published_live().order_by('published_from__order_id')
    return members

@register.assignment_tag
def members_for_page_exclude(page):
    if page.published_from:
        members = Member.objects.get_published_live().exclude(published_from__related_to_pages=page.published_from).order_by('published_from__order_id')
    else:
        members = Member.objects.get_published_live().exclude(published_from__related_to_pages=page).order_by('published_from__order_id')
    
    return members


@register.assignment_tag
def get_all_members(page=False):
	members = Member.objects.get_published_live().order_by('published_from__order_id')
	return members